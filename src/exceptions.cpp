/*
 * exceptions.cpp
 *
 *  Created on: 10 Feb 2019
 *      Author: sgasper
 */

#include "exceptions.h"

// ------------------------------------------------------------
CMDExecutionOnRouterException::CMDExecutionOnRouterException(string cmd,
		string cmdOut){
	this->cmd = cmd;
	this->cmdOut = cmdOut;
}

const char * CMDExecutionOnRouterException::what() const throw(){
	string msg = "command: " + cmd + "\ncommand output: " = cmdOut;
	return msg.c_str();
}

// ------------------------------------------------------------
DBTableMetaException::DBTableMetaException(string selectErr){
	this->err = selectErr;
}

const char * DBTableMetaException::what() const throw(){
	string msg = "unable to parse meta data of database table: " + err;
	return msg.c_str();
}

// ------------------------------------------------------------
/*
 * Generic exception for Debugnet operations
 */
DebugnetException::DebugnetException(string msg){
	this->msg = msg;
}

const char * DebugnetException::what() const throw(){
	return msg.c_str();
}

// ------------------------------------------------------------
FieldNotFoundInRecordException::FieldNotFoundInRecordException(string field,
		string opperation){
	this->field = field;
	this->opperation = opperation;
}

const char * FieldNotFoundInRecordException::what() const throw(){
	string msg = "field: " + field + ", operation: " + opperation;
	return msg.c_str();
}

// ------------------------------------------------------------
FileNotFoundException::FileNotFoundException(string fileDesc, string filePath){
	this->fileDesc = fileDesc;
	this->filePath = filePath;
}

const char * FileNotFoundException::what() const throw(){
	string msg = fileDesc + " ->  " + filePath;
	return msg.c_str();
}
// ------------------------------------------------------------
InsertToDBException::InsertToDBException(string insertErr){
	this->insertErr = insertErr;
}

const char * InsertToDBException::what() const throw(){
	return insertErr.c_str();
}

// ------------------------------------------------------------
LoadConfigurationException::LoadConfigurationException(string& configName,
		string& reason){
	this->configName = configName;
	this->reason = reason;
}

const char * LoadConfigurationException::what() const throw(){
	string msg = "unable to load configuration for \'" + configName + \
				 "\': " + reason;
	return msg.c_str();
}

// ------------------------------------------------------------
LoadDeviceException::LoadDeviceException(string& reason){
	this->reason = reason;
}

const char * LoadDeviceException::what() const throw(){
	return reason.c_str();
}

// ------------------------------------------------------------
SelectFromDBException::SelectFromDBException(string selectErr){
	this->selectErr = selectErr;
}

const char * SelectFromDBException::what() const throw(){
	return selectErr.c_str();
}

// ------------------------------------------------------------
UnsupportedDBDataType::UnsupportedDBDataType(string dbType){
	this->dbType = dbType;
}

const char * UnsupportedDBDataType::what() const throw(){
	string msg = "unsupported data type in database: " + dbType;
	return msg.c_str();
}

// ------------------------------------------------------------
UpdateDBException::UpdateDBException(string updateErr){
	this->updateErr = updateErr;
}

const char * UpdateDBException::what() const throw(){
	return updateErr.c_str();
}
