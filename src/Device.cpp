/*
 * Device.cpp
 *
 *  Created on: 11 Feb 2019
 *      Author: sgasper
 */

#include "devices.h"

/*
 * For representing 'NULL' type device object.
 */
Device::Device(){
	this->null = true;
}

/*
 * Constructor for taking a DBRecord instance which should already hold details
 * about the device.
 */
Device::Device(DBRecord dbRecord) {
	this->dbRecord = dbRecord;
}

/*
 * Constructor for creating the device from scratch
 */
Device::Device(string macAddress, string ipAddress, time_t firstConnected,
		string manufacturer) {
	dbRecord = DBRecord();
	// Set first time attributes
	dbRecord.setField("mac_address", macAddress);
	dbRecord.setField("first_connected", firstConnected);
	dbRecord.setField("manufacturer", manufacturer);
	// Default last connected time to first connected time
	setLastConnectedTime(getFirstConnectedTime());
	setIpAddress(ipAddress);
	// This constructor assumes the device is online
	setStatus(DEV_ONLINE);
}

/*
 * Remove record of hostname
 */
void Device::clearHostname(){
	dbRecord.removeField("hostname");
}

/*
 * Remove record of IP address
 */
void Device::clearIpAddress(){
	dbRecord.removeField("ip_address");
}

/*
 * Return the connection type of the device
 */
CONNECTION_TYPE Device::getConnectionType() {
	int connTypeInt; dbRecord.getField("connection_type", connTypeInt);

	// Convert integer ID to appropriate enum
	return connTypeIntIdToEnum(connTypeInt);
}

/*
 * Return DBRecord instance
 */
DBRecord Device::getDBRecord() {
	return dbRecord;
}

/*
 * Return time of first connection
 */
time_t Device::getFirstConnectedTime() {
	time_t time; dbRecord.getField("first_connected", time); return time;
}

/*
 * Return hostname
 */
string Device::getHostname() {
	string hostname;

	if (dbRecord.hasField("hostname") == true){
		dbRecord.getField("hostname", hostname);
	}
	else{
		// Hostname not set, show this as an empty string.
		hostname = "";
	}

	return hostname;
}

/*
 * Get the most human readable way of identifying the device
 */
string Device::getHumanReadableName(){
	// Ordered by most readable
	if (hasHostname()){
		return getHostname();
	}
	else if (hasManufacturer()){
		// <ip>(<manufacturer>)
		return getIpAddress() + "(" + getManufacturer() + ")";
	}
	else{
		return getIpAddress();
	}
}

/*
 * Return IP address
 */
string Device::getIpAddress() {
	string ip;

	if (dbRecord.hasField("ip_address") == true){
		dbRecord.getField("ip_address", ip);
	}
	else{
		// IP not set, show this as an empty string.
		ip = "";
	}

	return ip;
}

/*
 * Return the last known time the device was connected
 */
time_t Device::getLastConnectedTime() {
	time_t time; dbRecord.getField("last_connected", time); return time;
}

/*
 * Return the MAC address of the device
 */
string Device::getMacAddress() {
	string mac; dbRecord.getField("mac_address", mac); return mac;
}

/*
 * Return the manufacturer of the device
 */
string Device::getManufacturer() {
	string mf;

	if (dbRecord.hasField("manufacturer") == true){
		dbRecord.getField("manufacturer", mf);
	}
	else{
		// Manufacturer not set, show this as an empty string.
		mf = "";
	}

	return mf;
}

/*
 * Return the status off the device
 */
DEVICE_STATUS Device::getStatus() {
	int statusInt; dbRecord.getField("status", statusInt);

	// Convert status integer to appropriate enum
	return deviceStatusIntIdToEnum(statusInt);
}

/*
 * Check if the device has a hostname set
 */
bool Device::hasHostname() {
	return dbRecord.hasField("hostname") && getHostname() != "";
}

/*
 * Check if the device has a IP address set
 */
bool Device::hasIpAddress() {
	return dbRecord.hasField("ip_address") && getIpAddress() != "";
}

/*
 * Check the device has a manufacturer set
 */
bool Device::hasManufacturer() {
	return dbRecord.hasField("manufacturer") && getManufacturer() != "";
}

/*
 * Check if the device object is in a NULL state
 */
bool Device::isNull() {
	return null;
}

/*
 * Check if a device has wireless connectivity
 */
bool Device::isWireless(){
	CONNECTION_TYPE connType = getConnectionType();

	return  connType == CONN_WIFI_2_4 || connType == CONN_WIFI_5;
}

/*
 * Set the connection type
 */
void Device::setConnectionType(CONNECTION_TYPE connType) {
	dbRecord.setField("connection_type", connTypeEnumToIntId(connType));
}

/*
 * Set the hostname of the device
 */
void Device::setHostname(string hostname) {
	dbRecord.setField("hostname", hostname);
}

/*
 * Set the device IP Address
 */
void Device::setIpAddress(string ipAddress) {
	dbRecord.setField("ip_address", ipAddress);
}

/*
 * Set the last known time the device was connected
 */
void Device::setLastConnectedTime(time_t time) {
	dbRecord.setField("last_connected", time);
}

/*
 * Set the status of the device
 */
void Device::setStatus(DEVICE_STATUS status) {
	dbRecord.setField("status", deviceStatusEnumToIntId(status));
}

