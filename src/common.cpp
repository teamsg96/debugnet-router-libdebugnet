/*
 * common.cpp
 *
 *  Created on: 18 Jan 2019
 *      Author: sgasper
 */

#include "common.h"

// Convert bits to megabits
double bitsToMegaBits(int bits){
	return static_cast<double>(bits) / 1000000.0;
}

// Convert a byte value into bits
int bytesToBits(int bytes){
	return bytes * 8;
}

/*
 * Convert byte value to kilobytes
 */
double bytesToKilobytes(int bytes){
	return static_cast<double>(bytes) / 1000.0;
}

/*
 * Convert byte value to megabyte
 */
double bytesToMegabytes(int bytes){
	return static_cast<double>(bytes) / 1000000.0;
}

/*
 * Execute a command on the router and return its output and exit code.
 */
CommandResult executeCmdOnRouter(string cmd, bool supress){
	FILE * stream;
	char buffer[MAX_BUFFER];
	CommandResult result = {};

	// Store the command used
	result.cmd = cmd;

	// Redirect stderr to a file named 1.
	cmd.append(" 2>&1");

	if (supress == false){
		cout << "executing command on router: " << cmd << endl;
	}

	// Open a process by creating a pipe, forking and invoking the shell. The
	// pipe is setup in read mode.
	stream = popen(cmd.c_str(), "r");

	if (stream){
		// Don't stop until the end of the stream has been reached
		while (!feof(stream)){
			// Load data from stream into the buffer whilst buffer is not at
			// end of file and no errors have occurred.
			if (fgets(buffer, MAX_BUFFER, stream) != NULL){
				result.out.append(buffer);
			}
		}

		// Close the pipe and store the exit status
		result.exitCode = WEXITSTATUS(pclose(stream));
	}

	if (supress == false){
		cout << "command exit status: " << to_string(result.exitCode) << endl;
		cout << "command output: \n" << result.out << endl;
	}

	return result;
}

/*
 * Split comma delimited line of text
 */
vector<string> parseCsvLine(string line){
	// Split line by commas.
	regex seperator(",");
	vector<string> cols{sregex_token_iterator(
			line.begin(),
			line.end(),
			seperator,
			-1), {}};

	return cols;
}
