/*
 * database.cpp
 *
 *  Created on: 3 Feb 2019
 *      Author: sgasper
 */

#include "database.h"


/*
 * Execute SQL on a database using the database CLI
 */
CommandResult execSQL(string sqlToRun, bool includeHeader){
	string dbCmd;
	string optArgs;

	// Build the command string. DB command format:
	//	<cli_name> <db_file> <opt_args> "<SQL>"

	if (includeHeader == true){
		// Argument will display name of each column on the first row
		// of output.
		optArgs += "-header ";
	}

	// Argument will display string types in quotes
	optArgs += "-separator \'" + COL_SEPERATOR + "\' ";

	// Set time to wait until DB becomes unlocked (ms)
	optArgs += "-cmd \".timeout " + to_string(DB_LOCK_WAIT) + "\" ";

	// Assemble arguments
	dbCmd = DB_CLI_NAME + " " + DB_FILE + " " + optArgs + " \"" + \
			sqlToRun + "\"";

	// Execute SQL on the database.
	return executeCmdOnRouter(dbCmd);
}

/*
 * Generate a SQL select statement with the option of a condition.
 */
string generateSQLSelectStatement(string tableName, string condition){
	string statement;

	// Select statement should display all column values, including rowid which
	// is a built in column and is standard to all tables.
	statement = "SELECT rowid, * FROM " + tableName;

	// Apply condition if given
	if (condition != ""){
		statement += " WHERE " + condition;
	}

	return statement;
}

/*
 * Determine the data type of a database cell value by interrogating its
 * data structure.
 */
string getCellDataType(string tableName, string columnName, string rowId){
	CommandResult dbCmdResult;
	string dataType;
	string statement;

	// Use the 'typeof' instruction to get the DB CLI to tell us which of
	// its supported data types it believes the cell value to be.
	statement = "SELECT typeof(" + columnName + ") from " + tableName + \
				" where rowid=" + rowId + ";";

	dbCmdResult = execSQL(statement, false);

	// Raise an error if their was an issue executing the select statement
	if (dbCmdResult.exitCode != 0){
		throw SelectFromDBException(dbCmdResult.out);
	}

	dataType = dbCmdResult.out;

	// Remove leading new line character from the SQL output
	dataType.pop_back();

	// Convert each character in the data type name to upper case so that
	// its consist with the upper case data types of column types that
	// is used else where.
	for (auto& c: dataType) c = toupper(c);

	return dataType;
}

/*
 * Gets meta data for a particular table in the database.
 */
map<string, map<string, string>> getTableMeta(string tableName){
	CommandResult dbCmdResult;
	map<string, map<string, string>> tableMeta;
	string rawLine;
	string sqlStatement;

	// Statement will return all meta knowledge of table
	sqlStatement = "PRAGMA table_info(" + tableName + ");";

	// Execute SQL instruction
	dbCmdResult = execSQL(sqlStatement);

	// Raise an error if their was an issue fetching meta data for table
	if (dbCmdResult.exitCode != 0){
		throw DBTableMetaException(dbCmdResult.out);
	}

	istringstream outStream(dbCmdResult.out);

	// Fetch the first row of output which contains the fields of the meta
	// table. These are broken down into a list so that it can be iterated over.
	getline(outStream, rawLine);
	vector<string> metaFields = parseCsvLine(rawLine);

	// Iterate over each row in the meta table where each row defines the
	// meta data for each column in the table of interest
	while(getline(outStream, rawLine)){
		string colName;
		map<string, string> tableColMeta;

		vector<string> colAttribs = parseCsvLine(rawLine);

		// Iterate over each field in the row.
		for (unsigned int i=0; i < metaFields.size(); i++){
			tableColMeta[metaFields[i]] = colAttribs[i];

			// Keep a record of the column name in the meta row.
			if (metaFields[i] == "name"){
				colName = colAttribs[i];

			}
		}

		// Store column meta by its name
		tableMeta[colName] = tableColMeta;
	}

	return tableMeta;
}

/*
 * Store a record of data in the database.
 */
void insertToDB(string tableName, DBRecord record){
	string sql;

	sql = record.getAsSqlInsertStr(tableName);

	CommandResult dbCmdResult = execSQL(sql);

	// Raise an error if their was an issue inserting to the database
	if (dbCmdResult.exitCode != 0){
		throw InsertToDBException(dbCmdResult.out);
	}

}

/*
 * Store record(s) of data in the database.
 */
void insertToDB(string tableName, vector<DBRecord> records){
	string sql;

	// Format each row of data into an SQL insertion string
	for (DBRecord record: records){
		sql += record.getAsSqlInsertStr(tableName);
	}

	CommandResult dbCmdResult = execSQL(sql);

	// Raise an error if their was an issue inserting to the database
	if (dbCmdResult.exitCode != 0){
		throw InsertToDBException(dbCmdResult.out);
	}
}

/*
 * Parse the string output returned from the execution of an SQL statement
 */
vector<DBRecord> parseSelectOutput(string sqlOut, string tableName){
	istringstream outStream(sqlOut);
	map<string, map<string, string>> tableMeta;
	string rawLine;
	vector<DBRecord> parsedRows;

	// Get the meta data of the table
	tableMeta = getTableMeta(tableName);

	// Fetch the first row of output which contains the tables column names.
	// These are broken down into a list so that it can be iterated over
	// in parallel with row data later on.
	getline(outStream, rawLine);
	vector<string> colNames = parseCsvLine(rawLine);

	// Iterate over each row of data from the output.
	while(getline(outStream, rawLine)){
		DBRecord record;

		// Break up string data row into columns so that its in parallel with
		// the column names list.
		vector<string> colData = parseCsvLine(rawLine);

		for (unsigned int i=0; i < colData.size(); i++){
			string sqlDataType;

			// Determine the data type of the cell
			if (colNames[i] == "rowid"){
				// The row ID column does not have an record in the meta data,
				// as its a built in column. But we know that the data type of
				// a 'rowid' column is always Integer.
				sqlDataType = "INTEGER";
			}
			else if (tableMeta[colNames[i]]["type"] == "BLOB"){
				// Derive the data type of the cell if the column is of blob
				// type. Columns of blob type will allow data to be stored in
				// it of any type.
				sqlDataType = getCellDataType(tableName, colNames[i],
										 	  colData[0]);
			}
			else{
				// Use the data type allocated to column
				sqlDataType = tableMeta[colNames[i]]["type"];
			}

			// Add the cell data in its correct form to the record object
			if (colData[i] == "NULL"){
				// Don't store attribute if it has no data value
				continue;
			}
			else if (sqlDataType == "REAL"){
				record.setField(colNames[i], atof(colData[i].c_str()), true);
			}
			else if (sqlDataType == "INTEGER"){
				record.setField(colNames[i], atoi(colData[i].c_str()), true);
			}
			else if (sqlDataType == "TEXT"){
				record.setField(colNames[i], colData[i], true);
			}
			else{
				// Unable to understand data type, so raise an error.
				throw UnsupportedDBDataType(sqlDataType);
			}
		}

		parsedRows.push_back(record);
	}

	return parsedRows;
}

/*
 * Gets the average value of a field in a database that has set conditions.
 * If conversion was successful then 0 is returned, otherwise 1 is.
 */
int selectAvgFromDB(string tableName, string fieldName, double& avgVal,
		string condition){
	CommandResult dbCmdResult;
	string sqlStatement;

	// Build main part of statement
	sqlStatement = "SELECT avg(" + fieldName + ") FROM " + tableName;

	// Apply condition if given
	if (condition != ""){
		sqlStatement += " WHERE " + condition;
	}

	// Execute the select statement using the DB CLI
	dbCmdResult = execSQL(sqlStatement, false);

	// Raise and exception if a problem occurred selecting executing select
	if (dbCmdResult.exitCode != 0){
		throw SelectFromDBException(dbCmdResult.out);
	}

	// Check sqlite returned a value for the average. If not then dont perform
	// a conversion.
	regex doublePattern("\\d+.\\d+");
	smatch match;
	regex_search(dbCmdResult.out, match, doublePattern);
	if (match.size() != 0){
		// Convert to double type
		avgVal = stod(dbCmdResult.out);
		return 0;
	}
	else{
		return 1;
	}
}


/*
 * Select data from a table. To refine output a condition maybe used.
 */
vector<DBRecord> selectFromDB(string tableName, string condition){
	CommandResult dbCmdResult;
	string sqlStatement;
	vector<DBRecord> selectedData;

	// Build SQL select statement based on condition and table name
	sqlStatement = generateSQLSelectStatement(tableName, condition);

	// Execute the select statement using the DB CLI
	dbCmdResult = execSQL(sqlStatement);

	// Raise and exception if a problem occurred selecting executing select
	if (dbCmdResult.exitCode != 0){
		throw SelectFromDBException(dbCmdResult.out);
	}

	// Parse the response. Don't parse empty output as this is because the
	// select statement didn't find any data that met its criteria.
	if (dbCmdResult.out != ""){
		selectedData = parseSelectOutput(dbCmdResult.out, tableName);
	}

	return selectedData;
}

/*
 * Update DB records
 */
void updateDB(string tableName, vector<DBRecord> recordsToUpdate){
	string sql;

	for (DBRecord record: recordsToUpdate){
		sql += record.getAsSqlUpdateStr(tableName);
	}

	CommandResult dbCmdResult = execSQL(sql);

	// Raise an error if their was an issue updating the database
	if (dbCmdResult.exitCode != 0){
		throw UpdateDBException(dbCmdResult.out);
	}
}

