/*
 * common.h
 *
 *  Created on: 18 Jan 2019
 *      Author: sgasper
 */

#ifndef LIBDEBUGNET_COMMON_H_
#define LIBDEBUGNET_COMMON_H_

#include <iostream>
#include <regex>
#include <vector>
using namespace std;

const int MAX_BUFFER = 256;

const short int LINUX_EXIT_CODE_CMD_NOT_FOUND = 127;

// Stores the result of a command execution
struct CommandResult
{
	int exitCode;
	string cmd;
	string out;
};

double bitsToMegaBits(int bits);
int bytesToBits(int bytes);
double bytesToKilobytes(int bytes);
double bytesToMegabytes(int bytes);
CommandResult executeCmdOnRouter(string cmd, bool suppress = true);
vector<string> parseCsvLine(string line);


#endif /* LIBDEBUGNET_COMMON_H_ */
