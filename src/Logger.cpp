/*
 * Logger.cpp
 *
 *  Created on: 19 Feb 2019
 *      Author: sgasper
 */

#include "Logger.h"

Logger::Logger() {
}

/*
 * Log message to file
 */
void Logger::log(string msg, LOG_LEVEL level){
    char strTime[26];
	ofstream logFile;
    struct tm* tm_info;
	time_t now;

	logFile.open(logFilePath, ios_base::app);

	// Prefix message with warning or error if either level
	// has been given
	if (level == LOG_WARNING){
		logFile << "WARNING: ";
	}
	else if (level == LOG_ERROR){
		logFile << "ERROR: ";
	}

	// Set the current time
    time(&now);

    // Adjust to local time
    tm_info = localtime(&now);

    // Format time into human readable string
    strftime(strTime, BUFFER_TIME, "%Y-%m-%d %H:%M:%S", tm_info);

	logFile << strTime << ": " << msg << "\n";

	logFile.close();
}

/*
 * Log message to file with a handle before the message
 */
void Logger::log(string handle, string msg, LOG_LEVEL level){
	string msgWithHandle = handle + ": " + msg;

	log(msgWithHandle, level);
}

// Set the path of the log file
void Logger::setFile(string logFilePath){
	this->logFilePath = logFilePath;
}

