/*
 * database.h
 *
 *  Created on: 3 Feb 2019
 *      Author: sgasper
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <vector>
#include "common.h"
#include "exceptions.h"

using namespace std;

const string COL_SEPERATOR = ",";					// Denotes columns in data strings
const string DB_FILE = "~/debugnet/debugnet_db";	// Path to the database file
const string DB_CLI_NAME = "sqlite3";				// Name of command to execute DB CLI
const int DB_LOCK_WAIT = 30000;						// ms to wait for DB to become unlocked

class DBRecord {
private:
	map<string, int> intParams;
	map<string, double> realParams;
	map<string, string> strParams;
	vector<string> updatedFields;
public:
	virtual ~DBRecord() {};
	void setField(string name, double value, bool ignoreUpdate=false);
	void setField(string name, int value, bool ignoreUpdate=false);
	void setField(string name, time_t value, bool ignoreUpdate=false);
	void setField(string name, string value, bool ignoreUpdate=false);
	string getAsSqlInsertStr(string tableName);
	string getAsSqlUpdateStr(string tableName);
	void getField(string field, bool& value);
	void getField(string field, double& value);
	void getField(string field, int& value);
	void getField(string field, string& value);
	void getField(string field, time_t& value);
	bool hasField(string field);
	void markFieldAsUpdated(string field);
	void removeField(string field, bool ignoreUpdate=false);
};

CommandResult execSQL(string sqlToRun, bool includeHeader=true);
string generateSQLSelectStatement(string tableName, string condition="");
string getCellDataType(string tableName, string columnName, string rowId);
map<string, map<string, string>> getTableMeta(string tableName);
void insertToDB(string tableName, DBRecord record);
void insertToDB(string tableName, vector<DBRecord> records);
vector<DBRecord> parseSelectOutput(string sqlOut, string tableName);
int selectAvgFromDB(string tableName, string fieldName, double& avgVal,
		string condition="");
vector<DBRecord> selectFromDB(string tableName, string condition="");
void updateDB(string tableName, vector<DBRecord> recordsToUpdate);

#endif /* DATABASE_H_ */
