/*
 * exceptions.h
 *
 *  Created on: 10 Feb 2019
 *      Author: sgasper
 */

#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <iostream>
using namespace std;

class CMDExecutionOnRouterException: public exception{
private:
	string cmd;
	string cmdOut;
public:
	CMDExecutionOnRouterException(string cmd, string cmdOut);
	const char * what() const throw();
};

class DBTableMetaException: public exception{
private:
	string err;
public:
	DBTableMetaException(string err);
	const char * what() const throw();
};

class DebugnetException: public exception{
private:
	string msg;
public:
	DebugnetException(string msg);
	const char * what() const throw();
};

class FieldNotFoundInRecordException: public exception{
private:
	string field;
	string opperation;
public:
	FieldNotFoundInRecordException(string field, string opperation);
	const char * what() const throw();
};

class FileNotFoundException: public exception{
private:
	string fileDesc;
	string filePath;
public:
	FileNotFoundException(string fileDesc, string filePath);
	const char * what() const throw();
};

class InsertToDBException: public exception{
private:
	string insertErr;
public:
	InsertToDBException(string insertErr);
	const char * what() const throw();
};

class LoadConfigurationException: public exception{
private:
	string configName;
	string reason;
public:
	LoadConfigurationException(string& configName, string& reason);
	virtual const char* what() const throw();
};

class LoadDeviceException: public exception{
private:
	string reason;
public:
	LoadDeviceException(string& reason);
	virtual const char* what() const throw();
};

class SelectFromDBException: public exception{
private:
	string selectErr;
public:
	SelectFromDBException(string selectErr);
	const char * what() const throw();
};

class UnsupportedDBDataType: public exception{
private:
	string dbType;
public:
	UnsupportedDBDataType(string dbType);
	const char * what() const throw();
};

class UpdateDBException: public exception{
private:
	string updateErr;
public:
	UpdateDBException(string updateErr);
	const char * what() const throw();
};

#endif /* EXCEPTIONS_H_ */
