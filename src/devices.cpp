/*
 * devices.cpp
 *
 *  Created on: 12 Feb 2019
 *      Author: sgasper
 */

#include "devices.h"

/*
 * Add devices to the data base.
 */
void addDevices(vector<Device> devices){
	vector<DBRecord> dbRecords;

	for (Device device : devices){
		dbRecords.push_back(device.getDBRecord());
	}

	insertToDB(DEVICES_TABLE, dbRecords);
}

/*
 * Converts connection type enum to a unique integer ID
 */
int connTypeEnumToIntId(CONNECTION_TYPE connType){
	map<CONNECTION_TYPE, int> enumToInt = {
		{CONN_OTHER, 1},
		{CONN_NONE, 2},
		{CONN_WIFI_2_4, 3},
		{CONN_WIFI_5, 4}
	};

	return enumToInt[connType];
}

/*
 * Converts connection type from unique integer ID to enum
 */
CONNECTION_TYPE connTypeIntIdToEnum(int connTypeInt){
	map<int, CONNECTION_TYPE> intToEnum = {
		{1, CONN_OTHER},
		{2, CONN_NONE},
		{3, CONN_WIFI_2_4},
		{4, CONN_WIFI_5}
	};

	return intToEnum[connTypeInt];
}

// Convert all database records to device objects
vector<Device> convertDBRecordsToDevices(vector<DBRecord> records){
	vector<Device> devices;

	for (DBRecord record : records){
		devices.push_back(Device(record));
	}

	return devices;
}

/*
 * Converts device status enum to a unique integer ID
 */
int deviceStatusEnumToIntId(DEVICE_STATUS status){
	map<DEVICE_STATUS, int> enumToInt = {
		{DEV_OFFLINE, 1},
		{DEV_ONLINE, 2}
	};

	return enumToInt[status];
}

/*
 * Converts device status unique integer ID to enum
 */
DEVICE_STATUS deviceStatusIntIdToEnum(int statusInt){
	map<int, DEVICE_STATUS> intToEnum = {
		{1, DEV_OFFLINE},
		{2, DEV_ONLINE}
	};

	return intToEnum[statusInt];
}

/*
 * Return all devices in any 'status' with any 'connection type'
 */
vector<Device> getAllDevices(){
	vector<DBRecord> sqlResponse = selectFromDB(DEVICES_TABLE);

	return convertDBRecordsToDevices(sqlResponse);
}

/*
 * Return all 'online' devices
 */
vector<Device> getAllOnlineDevices(){
	string condition = "status=" + to_string(deviceStatusEnumToIntId(DEV_ONLINE));
	vector<DBRecord> sqlResponse = selectFromDB(DEVICES_TABLE, condition);

	return convertDBRecordsToDevices(sqlResponse);
}

/*
 * Return all 'online' wireless devices
 */
vector<Device> getAllOnlineWirelessDevices(){
	string condition;
	condition = "connection_type=" + to_string(connTypeEnumToIntId(CONN_WIFI_2_4));
	condition += " OR connection_type=" + to_string(connTypeEnumToIntId(CONN_WIFI_5));
	condition += " AND status=" + to_string(deviceStatusEnumToIntId(DEV_ONLINE));

	vector<DBRecord> sqlResponse = selectFromDB(DEVICES_TABLE, condition);

	return convertDBRecordsToDevices(sqlResponse);
}

/*
 * Fetch device from DB with a specific MAC address
 */
Device getDevice(string macAddress){
	string condition = "mac_address=\'" + macAddress + "\'";

	vector<DBRecord> sqlResponse = selectFromDB(DEVICES_TABLE, condition);

	// Handle database response
	if (sqlResponse.empty()){
		// Device wasn't found, so create a device in 'NULL' mode.
		return Device();
	}
	else if(sqlResponse.size() > 1){
		// As MAC is unique, size should never be > 1, so this acts as a sanity
		// check.
		string err = "more than one device entry exists with MAC: " + macAddress;
		throw LoadDeviceException(err);
	}
	else{
		// Device entry found, create a device object to represent the record
		// entity and return it.
		return Device(sqlResponse[0]);
	}
}

/*
 * Update device entries in the data base.
 */
void updateDevices(vector<Device> devicesToUpdate){
	vector<DBRecord> dbRecords;

	for (Device device : devicesToUpdate){
		dbRecords.push_back(device.getDBRecord());
	}

	updateDB(DEVICES_TABLE, dbRecords);
}
