/*
 * devices.h
 *
 *  Created on: 12 Feb 2019
 *      Author: sgasper
 */

#ifndef DEVICES_H_
#define DEVICES_H_

#include "database.h"
#include "tables.h"

// For representing how a device connects to the router
enum CONNECTION_TYPE{
	CONN_OTHER,
	CONN_NONE,
	CONN_WIFI_2_4,
	CONN_WIFI_5
};

// For representing if a device is online or offline in the network.
enum DEVICE_STATUS{
	DEV_OFFLINE,
	DEV_ONLINE
};

class Device {
private:
	bool null = false;
	DBRecord dbRecord;
public:
	Device();
	Device(DBRecord dbRecord);
	Device(string macAddress, string ipAddress, time_t firstConnected,
			string manufacturer);
	void clearHostname();
	void clearIpAddress();
	CONNECTION_TYPE getConnectionType();
	DBRecord getDBRecord();
	time_t getFirstConnectedTime();
	string getHostname();
	string getHumanReadableName();
	string getIpAddress();
	time_t getLastConnectedTime();
	string getMacAddress();
	string getManufacturer();
	DEVICE_STATUS getStatus();
	bool hasHostname();
	bool hasIpAddress();
	bool hasManufacturer();
	bool isNull();
	bool isWireless();
	void setConnectionType(CONNECTION_TYPE connType);
	void setHostname(string hostname);
	void setIpAddress(string ipAddress);
	void setLastConnectedTime(time_t time);
	void setStatus(DEVICE_STATUS status);
};

void addDevices(vector<Device> devices);
int connTypeEnumToIntId(CONNECTION_TYPE connType);
CONNECTION_TYPE connTypeIntIdToEnum(int connTypeInt);
vector<Device> convertDBRecordsToDevices(vector<DBRecord> records);
int deviceStatusEnumToIntId(DEVICE_STATUS status);
DEVICE_STATUS deviceStatusIntIdToEnum(int statusInt);
vector<Device> getAllDevices();
vector<Device> getAllOnlineDevices();
vector<Device> getAllOnlineWirelessDevices();
Device getDevice(string macAddress);
void updateDevices(vector<Device> devicesToUpdate);

#endif /* DEVICES_H_ */
