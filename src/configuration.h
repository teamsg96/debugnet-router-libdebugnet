/*
 * configuration.h
 *
 *  Created on: 5 Feb 2019
 *      Author: sgasper
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <iostream>
#include <vector>
#include "database.h"
#include "tables.h"
using namespace std;

class ConfigParameter{
private:
	DBRecord dbRecord;
public:
	ConfigParameter(DBRecord dbRecord);
	string getName();
	template<class T> T getValue();
};

ConfigParameter getConfig(string configName);

#endif /* CONFIGURATION_H_ */
