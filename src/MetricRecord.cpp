/*
 * MetricRecord.cpp
 *
 *  Created on: 9 Feb 2019
 *      Author: sgasper
 */

#include "metrics.h"

MetricRecord::MetricRecord(){
}

/*
 * This constructor is for creating a new data base record. Every metric record
 * entry must have the time field defined.
 */
MetricRecord::MetricRecord(time_t time){
	dbRecord = DBRecord();
	dbRecord.setField("time", time);
}

/*
 * Constructor for when all metric parameters are already stored in a database
 * record.
 */
MetricRecord::MetricRecord(DBRecord dbRecord){
	this->dbRecord = dbRecord;
}

/*
 * Add a parameter to the database record
 */
template<class T>
void MetricRecord::addParam(string name, T value){
	dbRecord.setField(name, value);
}

/*
 * Retrieve the DBRecord object
 */
DBRecord MetricRecord::getDBRecord(){
	return dbRecord;
}

/*
 * Return the value of a metric parameter
 */
template<class T>
T MetricRecord::getParam(string param){
	T value;

	dbRecord.getField(param, value);
	return value;
}

/*
 * Check a parameter exists
 */
bool MetricRecord::hasParam(string param){
	return dbRecord.hasField(param);
}

template void MetricRecord::addParam(string name, double value);
template void MetricRecord::addParam(string name, int value);
template void MetricRecord::addParam(string name, string value);
template double MetricRecord::getParam<double>(string param);
template int MetricRecord::getParam<int>(string param);
template string MetricRecord::getParam<string>(string param);

