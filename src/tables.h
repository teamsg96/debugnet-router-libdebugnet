/*
 * tables.h
 *
 *  Created on: 8 Mar 2019
 *      Author: sgasper
 */

#ifndef TABLES_H_
#define TABLES_H_

#include <iostream>
using namespace std;

const string BANDWIDTH_ISP_TABLE = "BandwidthISP";
const string CONFIGURATION_TABLE = "Configuration";
const string DEVICES_TABLE = "Devices";
const string TRAFFIC_LOAD_DEVICES_TABLE = "TrafficLoadDevices";
const string LATENCY_DEVICE_TABLE = "LatencyDevices";
const string LATENCY_ISP_TABLE = "LatencyISP";
const string TRAFFIC_LOAD_ENT_NET_TABLE = "TrafficLoadEntNet";
const string WIRELESS_SIGNAL_TABLE = "WirelessSignal";


#endif /* TABLES_H_ */
