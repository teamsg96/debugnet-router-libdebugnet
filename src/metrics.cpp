/*
 * metrics.cpp
 *
 *  Created on: 2 Feb 2019
 *      Author: sgasper
 */

#include "metrics.h"

/*
 * Gets the average value in a metric field within a given time range. Filters
 * can be applied to refine results used in calculation.
 */
int getAvgMetricFieldValue(string tableName, string fieldName, int fromTime,
		int toTime, double& avgVal, vector<string> filters){
	// Add time range to condition. This will ensure only values from this time
	// range are used in the average calculation.
	string condition = "time >=" + to_string(fromTime) + " AND time <= " + to_string(toTime);

	// Apply 'exact match' filters given
	for (string filter : filters){
		condition += " AND " + filter + "";
	}

	return selectAvgFromDB(tableName, fieldName, avgVal, condition);
}

/*
 * Gets the average value in a metric field within a given time range(s).
 * Filters can be applied to refine results used in calculation.
 */
int getAvgMetricFieldValue(string tableName, string fieldName,
		vector<tuple<int, int>> timeRanges, double& avgVal,
		vector<string> filters){
	string condition = "";

	// Add every time range given to the condition string
	bool first=true;
	for (tuple<int,int> &timeRange : timeRanges){
		if (!first){
			// The keyword 'OR' should separate every time range.
			condition += " OR ";
		}

		first = false;

		condition += "time >=" + to_string(get<0>(timeRange)) +
				" AND time <= " + to_string(get<1>(timeRange));
	}


	// Apply 'exact match' filters given
	for (string filter : filters){
		condition += " AND " + filter + "";
	}

	return selectAvgFromDB(tableName, fieldName, avgVal, condition);
}

/*
 * Save a metric record to the data base.
 */
void saveMetric(string tableName, MetricRecord record){
	// The DBRecord contains all the metric parameters from the MetricRecord.
	// DBRecord can also be understood by data base handling functions. So
	// the DBRecord is extracted and used to insert the metric parameters to
	// the database.
	insertToDB(tableName, record.getDBRecord());
}

/*
 * Save multiple metric records to the data base.
 */
void saveMetrics(string tableName, vector<MetricRecord> records){
	vector<DBRecord> dbRecords;

	// The DBRecord contains all the metric parameters from the MetricRecord.
	// DBRecord can also be understood by data base handling functions. So
	// the DBRecord is extracted from every MetricRecord object and used to
	// insert the metric parameters to the database.
	for (MetricRecord mRecord : records){
		dbRecords.push_back(mRecord.getDBRecord());
	}

	insertToDB(tableName, dbRecords);
}
