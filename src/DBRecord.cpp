/*
 * DBRecord.cpp
 *
 *  Created on: 2 Feb 2019
 *      Author: sgasper
 */

#include "database.h"

/*
 * Set a field and its value of type double
 */
void DBRecord::setField(string name, double value, bool ignoreUpdate){
	realParams[name] = value;
	if (ignoreUpdate == false){
		markFieldAsUpdated(name);
	}
}

/*
 * Set a field and its value of type int
 */
void DBRecord::setField(string name, int value, bool ignoreUpdate){
	intParams[name] = value;
	if (ignoreUpdate == false){
		markFieldAsUpdated(name);
	}
}

/*
 * Set a field and its value of type time_t
 */
void DBRecord::setField(string name, time_t value, bool ignoreUpdate){
	intParams[name] = static_cast<int>(value);
	if (ignoreUpdate == false){
		markFieldAsUpdated(name);
	}
}

/*
 * Set a field and its value of type string
 */
void DBRecord::setField(string name, string value, bool ignoreUpdate){
	strParams[name] = value;
	if (ignoreUpdate == false){
		markFieldAsUpdated(name);
	}
}

/*
 * Format all fields and values into a SQL insert string
 */
string DBRecord::getAsSqlInsertStr(string tableName){
	string allColNamesStr;
	string allColValuesStr;
	string columnDelimiter = ",";

	// Add the name of every column from every data type as well as the value
	// for each column field. Every value must be converted into a string.
	for (auto const &param : intParams){
		allColNamesStr += param.first + ",";
		allColValuesStr += to_string(param.second) + ",";
	}

	for (auto const &param : realParams){
		allColNamesStr += param.first + ",";
		allColValuesStr += to_string(param.second) + ",";
	}

	for (auto const &param : strParams){
		allColNamesStr += param.first + ",";
		// String data types in SQL should be surrounded with quotes.
		allColValuesStr += "\'" + param.second + "\'" + ",";
	}

	// Remove trailing comma
	allColNamesStr.pop_back();
	allColValuesStr.pop_back();

	return "INSERT INTO " + tableName + "(" + allColNamesStr + ") " + \
			"VALUES (" + allColValuesStr + ");";
}

/*
 * Format all fields and values into a SQL update string
 */
string DBRecord::getAsSqlUpdateStr(string tableName){
	string updatedData = "";
	string columnDelimiter = ",";

	// Fetch only the fields that have been updated since the last DB update
	for (string fieldName: updatedFields){
		if (intParams.count(fieldName) != 0){
			updatedData += fieldName + " = " + to_string(intParams[fieldName]) + columnDelimiter;
		}
		else if (realParams.count(fieldName) != 0){
			updatedData += fieldName + " = " + to_string(realParams[fieldName]) + columnDelimiter;
		}
		else if (strParams.count(fieldName) != 0){
			// String data types in SQL should be surrounded with quotes.
			updatedData += fieldName + " = " + "\'" + strParams[fieldName] + "\'" + columnDelimiter;
		}
		else{
			// If the field has been updated but no value is found then the
			// value is NULL.
			updatedData += fieldName + " = " + " NULL" + columnDelimiter;
		}
	}
	// Remove trailing comma
	updatedData.pop_back();

	string rowId;
	getField("rowid", rowId);

	return "UPDATE " + tableName + " SET " + updatedData + \
			" WHERE rowid=" + rowId + ";";
}

/*
 * Get a field value as type bool
 */
void DBRecord::getField(string field, bool& value){
	// Bool types in the DB are stored as integers. So convert from int to bool
	if (intParams.count(field)){
		value = intParams[field];
	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(field, "get bool field");
	}
}

/*
 * Get a field value as type double
 */
void DBRecord::getField(string field, double& value){
	// Determine cast/converting method based on what the field value is
	// currently stored as.
	if (intParams.count(field)){
		// Cast to double
		value = static_cast<double>(intParams[field]);
	}
	else if (realParams.count(field)){
		// Already in double format
		value = realParams[field];
	}
	else if (strParams.count(field)){
		// Cast to double
		value = stod(strParams[field]);
	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(field,  "get double field");
	}
}

/*
 * Get a field value as type int
 */
void DBRecord::getField(string field, int& value){
	// Determine cast/converting method based on what the field value is
	// currently stored as.
	if (intParams.count(field)){
		// Already integer
		value = intParams[field];
	}
	else if (realParams.count(field)){
		// Cast to integer
		value = static_cast<int>(realParams[field]);
	}
	else if (strParams.count(field)){
		// Cast to integer
		value = stoi(strParams[field]);
	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(field,  "get int field");
	}
}

/*
 * Get a field value that as type string
 */
void DBRecord::getField(string field, string& value){
	// Determine cast/converting method based on what the field value is
	// currently stored as.
	if (intParams.count(field)){
		// Cast to string
		value = to_string(intParams[field]);
	}
	else if (realParams.count(field)){
		// Cast to string
		value = to_string(realParams[field]);
	}
	else if (strParams.count(field)){
		// Already string
		value = strParams[field];
	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(field,  "get string field");
	}
}

/*
 * Get a field value as type time
 */
void DBRecord::getField(string field, time_t& value){
	// Determine cast/converting method based on what the field value is
	// currently stored as.
	if (intParams.count(field)){
		value = static_cast<time_t>(intParams[field]);
	}
	// Casting from other types not currently necessary
//	else if (realParams.count(field)){
//	}
//	else if (strParams.count(field)){
//	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(field,  "get time field");
	}
}

/*
 * Checks if a field has been defined in the record.
 */
bool DBRecord::hasField(string field){
	if (intParams.count(field) || realParams.count(field) ||
			strParams.count(field)){
		return true;
	}
	else{
		return false;
	}
}

/*
 * Mark field as having its value updated since last DB update
 */
void DBRecord::markFieldAsUpdated(string name){
	// Don't add if to the list if its already been marked as updated
	if (find(updatedFields.begin(), updatedFields.end(), name) == updatedFields.end()){
		updatedFields.push_back(name);
	}
}

/*
 * Remove field from the record
 */
void DBRecord::removeField(string name, bool ignoreUpdate) {
	if (intParams.count(name)){
		intParams.erase(name);
	}
	else if (realParams.count(name)){
		realParams.erase(name);
	}
	else if (strParams.count(name)){
		strParams.erase(name);
	}
	else{
		// Field not found
		throw FieldNotFoundInRecordException(name,  "remove field");
	}

	if (ignoreUpdate == false){
		markFieldAsUpdated(name);
	}
}
