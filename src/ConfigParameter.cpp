/*
 * ConfigurationParameter.cpp
 *
 *  Created on: 9 Feb 2019
 *      Author: sgasper
 */

#include "configuration.h"

/*
 * Initialise with the DBRecord object which this class wraps around to
 * make the interface to the DBRecord object more usable.
 */
ConfigParameter::ConfigParameter(DBRecord dbRecord){
	this->dbRecord = dbRecord;
}

/*
 * Return the name of the configuration parameter
 */
string ConfigParameter::getName(){
	string name;
	dbRecord.getField("name", name);

	return name;
}

/*
 * Return the value of the configuration parameter
 */
template<class T>
T ConfigParameter::getValue(){
	T value;

	dbRecord.getField("value", value);
	return value;
}


template double ConfigParameter::getValue<double>();
template int ConfigParameter::getValue<int>();
template string ConfigParameter::getValue<string>();
