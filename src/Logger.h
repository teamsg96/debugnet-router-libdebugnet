/*
 * Logger.h
 *
 *  Created on: 19 Feb 2019
 *      Author: sgasper
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <map>
#include <iostream>
#include <fstream>
using namespace std;

const int BUFFER_TIME = 26;
enum LOG_LEVEL{
	LOG_DEBUG = 0,
	LOG_INFO = 1,
	LOG_WARNING = 2,
	LOG_ERROR = 4
};

class Logger {
private:
	string logFilePath;
public:
	Logger();
	void log(string handle, string msg, LOG_LEVEL level=LOG_INFO);
	void log(string msg, LOG_LEVEL level=LOG_INFO);
	void setFile(string logFilePath);
};

#endif /* LOGGER_H_ */
