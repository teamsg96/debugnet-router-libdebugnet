/*
 * metrics.h
 *
 *  Created on: 2 Feb 2019
 *      Author: sgasper
 */

#ifndef METRICS_H_
#define METRICS_H_

#include <iostream>
using namespace std;

#include "database.h"

class MetricRecord {
private:
	DBRecord dbRecord;
public:
	MetricRecord();
	MetricRecord(DBRecord dbRecord);
	MetricRecord(time_t time);
	template<class T> void addParam(string name, T value);
	DBRecord getDBRecord();
	template<class T> T getParam(string param);
	bool hasParam(string param);
};

int getAvgMetricFieldValue(string tableName, string fieldName, int fromTime,
		int toTime, double& avgVal, vector<string> filters={});
int getAvgMetricFieldValue(string tableName, string fieldName,
		vector<tuple<int, int>> timeRanges, double& avgVal,
		vector<string> filters={});
void saveMetric(string tableName, MetricRecord record);
void saveMetrics(string tableName, vector<MetricRecord> records);
#endif /* METRICS_H_ */

