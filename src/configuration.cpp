/*
 * configuration.cpp
 *
 *  Created on: 5 Feb 2019
 *      Author: sgasper
 */

#include "configuration.h"

/*
 * Get specific configuration from the database
 */
ConfigParameter getConfig(string configName){
	string selectCondition;

	// The condition should ensure only the specific configuration
	// row is returned.
	selectCondition = "name=\'" + configName + "\'";

	// Select configuration data from the database
	vector<DBRecord> sqlResponse = selectFromDB(CONFIGURATION_TABLE,
												selectCondition);

	// Handle any issues with the response
	if (sqlResponse.empty() || sqlResponse.size() > 1){
		string reason;

		if (sqlResponse.empty()){
			reason = "configuration parameter not found in database";
		}
		else{
			reason = "more than one database entry exists for the " \
					 "configuration parameter";
		}

		throw LoadConfigurationException(configName, reason);
	}

	// The DBRecord object holds the specific configuration data and
	// the ConfigParameter knows how to read and update it for
	// 'configuration' purposes. The ConfigParameter class acts as an
	// interface and exposes all relevant configuration handling
	// functionality.
	return ConfigParameter(sqlResponse[0]);
}
